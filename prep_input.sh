#!/usr/bin/env bash
set -e

echo "Preparing input"
cd /data/resources/input

mkdir -p tmp/
echo "Unzip"
pv input.r1.fastq.gz | gunzip > tmp/input.r1.fastq
pv input.r2.fastq.gz | gunzip > tmp/input.r2.fastq

echo "Subsampling (R1)"
seqtk sample -s100 tmp/input.r1.fastq 250000 | gzip > small/input.r1.fastq.gz

echo "Subsampling (R2)"
seqtk sample -s100 tmp/input.r2.fastq 250000 | gzip > small/input.r2.fastq.gz

echo "Downloading reference genome..."
if [ ! -f Homo_sapiens_assembly19.fasta.gz]; then
    echo "Downloading reference genome..."
    wget https://s3.amazonaws.com/mgh.cider/cider_resources/Homo_sapiens_assembly19.fasta.gz
    pv Homo_sapiens_assembly19.fasta.gz | gunzip > Homo_sapiens_assembly19.fasta
    bwa index Homo_sapiens_assembly19.fasta
else
    echo "Skipping reference genome: already exists"
fi

echo "Cleaning up..."
rm -rvf ./tmp/

echo "Done"

