#!/usr/bin/env python3

import pandas as pd
import socket
import os
import urllib.parse
from urllib.parse import urlencode, quote_plus

from argparse import ArgumentParser
from io import StringIO

import re
import vcf
import numpy as np
from collections import Counter

def reorder_columns(df, col_order):
    def dedup(lst):
        sofar = []
        for x in lst:
            if x not in sofar:
                sofar.append(x)

        return sofar

    other_cols = [col for col in df.columns if col not in col_order]
    return pd.DataFrame(df, columns=dedup(col_order + other_cols), copy=True)

def read_vep(path):
    print("Parsing VCF: " + path)
    reader = vcf.Reader(filename=path)

    csq_desc = reader.infos['CSQ'].desc
    m = re.match(r".*Format:\s*(.*)$", csq_desc)

    if not m:
        raise ValueError("Bad VEP CSQ spec: {}".format(csq_desc))

    vep_fields = m.group(1).split('|')

    records = []
    for idx, record in enumerate(reader):
        row_data = {
            prop: getattr(record, prop) for prop in
            ('CHROM', 'POS', 'ID', 'REF', 'ALT', 'QUAL')
        }

        row_data['ALT'] = ", ".join(str(x) for x in row_data['ALT'])

        info = record.INFO
        ref_r1, ref_r2, alt_r1, alt_r2 = info['DP4']

        row_data['ALT_DP'] = alt_r1 + alt_r2
        row_data['REF_DP'] = ref_r1 + ref_r2
        row_data['DP'] = sum(info['DP4'])
        row_data['ALT_AF'] = info['AF']

        csq, = info['CSQ']
        csq = csq.split('|')

        if not len(csq) == len(vep_fields):
            raise ValueError("Invalid number of VEP fields: {}, "
                             "expected {}".format(len(csq), len(vep_fields)))

        row_data.update(dict(zip(vep_fields, csq)))

        records.append(row_data)

    df = pd.DataFrame(records)
    df = df.dropna(how='all', axis=1)

    df = reorder_columns(df, ["IGV", "CHROM", "POS", "SYMBOL", "REF", "ALT", "DP", "REF_DP", "ALT_DP", "ALT_AF",
                              "Consequence", "VARIANT_CLASS"] + vep_fields)

    df = df.rename(columns={
        'AF': '1000G_AF',
        'AFR_AF': '1000G_AFR_AF',
        'AMR_AF': '1000G_AMR_AF',
        'ASN_AF': '1000G_ASN_AF',
        'EUR_AF': '1000G_EUR_AF',
        'EAS_AF': '1000G_EAS_AF',
        'SAS_AF': '1000G_SAS_AF'
    })

    return df

def get_igv_link(bam_path, home_path, user, location):
    rel_path = bam_path.replace(home_path, "")
    url = "/".join(["http://hms-teaching.cidilab.org", user, rel_path])

    params = {'file': url,
              'merge': 'false',
              'genome': 'hg19',
              'locus': location}
    return "http://localhost:60151/load?" + urlencode(params, quote_via=quote_plus)


def main():
    """Main function"""
    parser = ArgumentParser(description="Converts VEP output to an Excel spreadsheet")
    parser.add_argument("input", help="VEP output file VCF")
    parser.add_argument("bam", help="BAM path")
    parser.add_argument("output", help="Output file")
    parsed_args = parser.parse_args()

    df = read_vep(parsed_args.input)
    df['IGV'] = [get_igv_link(parsed_args.bam, os.environ['HOME'], os.environ['USER'],
                              '{}:{}'.format(row['CHROM'], row['POS']))
                 for _, row in df.iterrows()]
    df.to_excel(parsed_args.output, index=False)


if __name__ == "__main__":
    main()
