#!/usr/bin/env bash
set -e

FQ_R1="$1"
FQ_R2="$2"
OUT="$3"

RESOURCES=/data/resources

if [ -z "$FQ_R1" ] || [ -z "$FQ_R2" ] || [ -z "$OUT" ]; then
    echo "Usage: "
    echo "  $0 r1.fastq.gz r2.fastq.gz output_directory"
    exit 1
fi

if [ ! -e "$FQ_R1" ]; then
    echo "$FQ_R1 doesn't exist"
    exit 1
fi

if [ ! -e "$FQ_R2" ]; then
    echo "$FQ_R2 doesn't exist"
    exit 1
fi

echo "Inputs:"
echo "  R1: $FQ_R1"
echo "  R2: $FQ_R2"

mkdir -p $OUT
echo "Saving output in $OUT"

function header {
    echo
    echo
    div="--------------------------------------------"
    echo $div
    echo $1
    echo $div
}

#########################
# QC
#########################
header "Raw sequence QC"
if [ ! -f $OUT/qc/input.r2_fastqc.zip ]; then
    mkdir -p $OUT/qc
    fastqc  $FQ_R1 $FQ_R2 --outdir=$OUT/qc
fi


#########################
# Alignment
#########################
header "Alignment"
if [ ! -f $OUT/aligned.sam ]; then
    bwa mem -t 2 -a -M -O 6 $RESOURCES/hg19/Homo_sapiens_assembly19.fasta $FQ_R1 $FQ_R2 > $OUT/aligned.sam
fi


#########################
# Clean SAM
#########################
header "Clean SAM"
if [ ! -f $OUT/aligned.clean.sam ]; then
    java  -Xmx4G  -jar $RESOURCES/tools/picard.jar CleanSam INPUT=$OUT/aligned.sam OUTPUT=$OUT/aligned.clean.sam
fi


#########################
# SAM to BAM
#########################
header "SAM -> BAM..."
if [ ! -f $OUT/aligned.sorted.bam ]; then
    java -Xmx4G -jar $RESOURCES/tools/picard.jar SortSam INPUT=$OUT/aligned.clean.sam OUTPUT=$OUT/aligned.sorted.bam SORT_ORDER=coordinate CREATE_INDEX=true
fi


#########################
# Add read groups
#########################
header "Add read groups"
if [ ! -f $OUT/aligned.read_grp_modified.bam ]; then
    java -Xmx4G -jar $RESOURCES/tools/picard.jar AddOrReplaceReadGroups \
         INPUT=$OUT/aligned.sorted.bam \
         OUTPUT=$OUT/aligned.read_grp_modified.bam \
         SORT_ORDER=coordinate \
         RGID=TEST-SAMPLE RGLB=TEST-SAMPLE RGPL=Illumina RGSM=TEST-SAMPLE RGPU=TEST-SAMPLE \
         CREATE_INDEX=true
fi


#########################
# Indel Realigner
#########################
header "GATK indel realigner"
if [ ! -f $OUT/aligned.gatk_indel_realigned.bam ]; then
    java -Xmx4G  -jar $RESOURCES/tools/GenomeAnalysisTK.jar -T IndelRealigner \
         -U -R $RESOURCES/hg19/Homo_sapiens_assembly19.fasta \
         -I $OUT/aligned.read_grp_modified.bam \
         -o $OUT/aligned.gatk_indel_realigned.bam \
         -compress 0 --filter_bases_not_stored --defaultBaseQualities 0 \
         -targetIntervals $RESOURCES/Homo_sapiens_assembly19.indel_cleaner_20120518.intervals \
         -model KNOWNS_ONLY \
         -maxInMemory 1000000 \
         -LOD 0.4 \
         -known $RESOURCES/Homo_sapiens_assembly19.known_indels_20120518.vcf
fi


#########################
# Base Recalibrator
#########################
if [ ! -f $OUT/aligned.gatk_base_recalibrated.bam ] && [[ $* == *--recalibrate* ]]; then
    header "Base recalibrator"
    java -Xmx4G -jar $RESOURCES/tools/GenomeAnalysisTK.jar -T BaseRecalibrator \
         -R $RESOURCES/hg19/Homo_sapiens_assembly19.fasta \
         -I $OUT/aligned.gatk_indel_realigned.bam \
         -L $RESOURCES/variantplex_03012017_targets.bed  \
         --num_cpu_threads_per_data_thread 1 \
         -knownSites $RESOURCES/Homo_sapiens_assembly19.dbsnp135.vcf \
         -knownSites $RESOURCES/Homo_sapiens_assembly19.known_indels_20120518.vcf \
         -o $OUT/base_recalibration.table

    java -jar $RESOURCES/tools/GenomeAnalysisTK.jar -T PrintReads \
         -I $OUT/aligned.gatk_indel_realigned.bam \
         -R $RESOURCES/hg19/Homo_sapiens_assembly19.fasta \
         -BQSR $OUT/base_recalibration.table \
         -o $OUT/aligned.gatk_base_recalibrated.bam

    cp $OUT/aligned.gatk_base_recalibrated.bam $OUT/aligned.pre_dup.bam
    samtools index $OUT/aligned.pre_dup.bam
fi

if [[ ! $* == *--recalibrate* ]]; then
    echo "Skipping base recalibrator"
    cp -v $OUT/aligned.gatk_indel_realigned.bam $OUT/aligned.pre_dup.bam
fi


#########################
# Mark duplicates
#########################
header "Mark duplicates"
if [ ! -f $OUT/aligned.final.bam ]; then
    java -Xmx4G -jar $RESOURCES/tools/picard.jar MarkDuplicates \
         INPUT=$OUT/aligned.pre_dup.bam \
         OUTPUT=$OUT/aligned.final.bam \
         REMOVE_DUPLICATES=false \
         METRICS_FILE=$OUT/duplicate_metrics.txt \
         CREATE_INDEX=true
fi


#########################
# BED coverage
#########################
header "BED coverage"
if [ ! -f $OUT/coverage.txt ]; then
    bedtools coverage -mean -a $RESOURCES/variantplex_03012017_coverage.bed -b $OUT/aligned.final.bam | sed 's/\r//g' > $OUT/coverage.txt
fi



#########################
# LoFreq variant calling
#########################
header "LoFreq variant caller"
if [ ! -f $OUT/variants.vcf ]; then
    lofreq call \
           --ref $RESOURCES/hg19/Homo_sapiens_assembly19.fasta \
           --bed  $RESOURCES/variantplex_03012017_targets.bed \
           --call-indels \
           --use-orphan \
           $OUT/aligned.final.bam > $OUT/variants.vcf
fi


header "VEP annotation"
if [ ! -f $OUT/variants.annotated.vcf ]; then
    perl $RESOURCES/tools/ensembl-vep/vep -i $OUT/variants.vcf \
         --assembly GRCh37 \
         --vcf \
         -o $OUT/variants.annotated.vcf \
         --everything \
         --force_overwrite \
         --hgvs \
         --fasta $RESOURCES/Homo_sapiens.GRCh37.75.dna.primary_assembly.fa \
         --dir $RESOURCES/.vep \
         --no_stats \
         --force_overwrite \
         --offline \
         --cache \
         --pick_order canonical,appris,tsl,biotype,ccds,rank,length \
         --pick

    python3 /data/resources/tools/vep_to_excel.py \
            $OUT/variants.annotated.vcf \
            $OUT/aligned.final.bam \
            $OUT/variants.annotated.xlsx
fi


header "Store final output"
mkdir -p $OUT/final
cp -rv $OUT/qc $OUT/final
cp -rv $OUT/duplicate_metrics.txt $OUT/final
cp -rv $OUT/coverage.txt $OUT/final
cp -rv $OUT/aligned.final.bam $OUT/final
cp -rv $OUT/aligned.final.bai $OUT/final
cp -rv $OUT/variants.annotated.vcf $OUT/final
cp -rv $OUT/variants.annotated.xlsx $OUT/final

echo -e "\n\n------------------"
echo "Analysis complete: $OUT/final"
