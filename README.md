# Bioinformatics Lab

## Introduction

In this lab you will analyze real sequencing data in order to detect germline & somatic variants. We will guide you through all the key computational steps, including:

* QC
* Alignment
* Variant calling
* Annotation with Variant Effect Predictor (VEP)
* Visualization & interpretation of results

These steps closely mimic the clinical solid tumor pipeline at the MGH Center for Integrated Diagnostics.

## Data & compute environment

You will be working with 500,000 (250,000 forward and 250,000 reverse) 2x151 bp sequencing reads from an Illumina NextSeq sequencer. 

We set up a Virtual Machine preloaded with bioinformatics tools, including:

* FastQC v0.11.5
* BWA 0.7.17
* GATK nightly-2016-01-24
* Picard Tools 1.128
* lofreq 2.1.3.1


## Connecting to the Teaching VM

### macOS/Linux
If using macOS/Linux, open the terminal app and type: 
```
#!bash
ssh groupX@hms-teaching.cidilab.org
```
Password will be provide in class.

### Windows
On Windows, download and run PuTTY [https://www.putty.org/](https://www.putty.org/), then input the following connection parameters:

* Host name: hms-teaching.cidilab.org
* User name: groupX
* Password: will be provided in class

### Once connected
Once connected, you will be presented with the bash prompt similar to the one below. If you get an error or see anything else, let us know. 

```
Welcome to Ubuntu 18.04.1 LTS (GNU/Linux 4.15.0-1027-aws x86_64)

<welcome message>

Last login: Mon Nov 26 15:31:27 2018 from 132.183.13.73
group12@hms_teaching:~$
```

## Setup

Let's start by defining some variables for convenience. This way we don't have to type the full path every time.

We will also create a directory to store pipeline output.

```
FQ_R1="/data/resources/input/small/input.r1.fastq.gz"
FQ_R2="/data/resources/input/small/input.r2.fastq.gz"

RESOURCES=/data/resources
OUT=$HOME/pipeline_out

mkdir -p $OUT
```

## Monitoring compute resources

During each step of the pipeline, you may open up a second ssh terminal to view the amount of resources (CPU, RAM, runtime) used.

```
htop -u groupX
```

## Computing raw sequencing metrics

Sometimes, it is informative to globally check on the quality and quantity of the sequencing output.  FastQC assesses the input fastq files regardless of where they align.
```
mkdir -p $OUT/qc
fastqc  $FQ_R1 $FQ_R2 --outdir=$OUT/qc
```

You can view the results by navigating to [http://hms-teaching.cidilab.org/groupX/pipeline_out](http://hms-teaching.cidilab.org/).

More details about FastQC here [http://www.bioinformatics.babraham.ac.uk/projects/fastqc/](http://www.bioinformatics.babraham.ac.uk/projects/fastqc/).

## Alignment

We will use BWA-MEM to perform alignment against the hg19 reference genome, with the following arguments:

* `-t 2`: parallelize with 2 threads
* `-a`: output all alignments for SE or unpaired PE
* `-M`: mark shorter split hits as secondary
* `-O 6`: set gap open penalty for deletions and insertions

```
bwa mem -t 2 -a -M -O 6 $RESOURCES/hg19/Homo_sapiens_assembly19.fasta $FQ_R1 $FQ_R2 > $OUT/aligned.sam
```

More details about BWA-MEM and parameters here [http://bio-bwa.sourceforge.net/bwa.shtml](http://bio-bwa.sourceforge.net/bwa.shtml). 

## Clean SAM
Picard Tools is a popular software to manipulate SAM/BAM/CRAM/vcf files.  More details here [https://broadinstitute.github.io/picard/](https://broadinstitute.github.io/picard/).

Next, we will call Picard to clean the resulting SAM, soft-clipping beyond-end-of-reference alignments and setting MAPQ to 0 for unmapped reads.

```
java  -Xmx4G  -jar $RESOURCES/tools/picard.jar CleanSam INPUT=$OUT/aligned.sam OUTPUT=$OUT/aligned.clean.sam
```

## Sort SAM and convert to BAM

We will now sort the aligned reads by coordinate. Note that the output is a BAM file, which is a more compact binary representation of SAM.

From the Picard documentation: "read alignments are sorted first by the reference sequence name (RNAME) field using the reference sequence dictionary (@SQ tag). 
Alignments within these subgroups are secondarily sorted using the left-most mapping position of the read (POS). 
Subsequent to this sorting scheme, alignments are listed arbitrarily."

```
java -Xmx4G -jar $RESOURCES/tools/picard.jar SortSam INPUT=$OUT/aligned.clean.sam OUTPUT=$OUT/aligned.sorted.bam SORT_ORDER=coordinate CREATE_INDEX=true
```

## Add read groups

While we don't have actual read groups, certain bioinformatics tools (e.g. GATK) require that they be defined. The command below will assign all reads to the same group (in this case all reads belonging to one sample).

In more sophisticated pipelines, you might use read groups to denote related reads, e.g. sharing the same molecular barcode/unique molecular index (UMI).

```
java -Xmx4G -jar $RESOURCES/tools/picard.jar AddOrReplaceReadGroups \
         INPUT=$OUT/aligned.sorted.bam \
         OUTPUT=$OUT/aligned.read_grp_modified.bam \
         SORT_ORDER=coordinate \
         RGID=TEST-SAMPLE RGLB=TEST-SAMPLE RGPL=Illumina RGSM=TEST-SAMPLE RGPU=TEST-SAMPLE \
         CREATE_INDEX=true
```

## Realign indels

Short read aligners struggle to map reads with gaps.  We will call GATK to locally adjust alignments based on the presence of insetions/deletions that are known in the population, 
aiming to minimize the total number of mismatches with respect to the reference genome.

From the GATK documentation:

"The local realignment process is designed to consume one or more BAM files and to locally 
realign reads such that the number of mismatching bases is minimized across all the reads. 
In general, a large percent of regions requiring local realignment are due to the presence of an 
insertion or deletion (indels) in the individual's genome with respect to the reference genome. 
Such alignment artifacts result in many bases mismatching the reference near the misalignment, which are 
easily mistaken as SNPs. Moreover, since read mapping algorithms operate on each read independently, 
it is impossible to place reads on the reference genome such at mismatches are minimized across all reads. 
Consequently, even when some reads are correctly mapped with indels, reads covering the indel near just the start 
or end of the read are often incorrectly mapped with respect the true indel, also requiring realignment. 
Local realignment serves to transform regions with misalignments due to indels into clean reads 
containing a consensus indel suitable for standard variant discovery approaches."

More details here [https://software.broadinstitute.org/gatk/](https://software.broadinstitute.org/gatk/).

We tell GATK to only realign only in the target intervals we specify (`Homo_sapiens_assembly19.indel_cleaner_20120518.intervals`), 
using only known indels (`$RESOURCES/Homo_sapiens_assembly19.known_indels_20120518.vcf`).


```
java -Xmx4G  -jar $RESOURCES/tools/GenomeAnalysisTK.jar -T IndelRealigner \
         -U -R $RESOURCES/hg19/Homo_sapiens_assembly19.fasta \
         -I $OUT/aligned.read_grp_modified.bam \
         -o $OUT/aligned.gatk_indel_realigned.bam \
         -compress 0 --filter_bases_not_stored --defaultBaseQualities 0 \
         -targetIntervals $RESOURCES/Homo_sapiens_assembly19.indel_cleaner_20120518.intervals \
         -model KNOWNS_ONLY \
         -maxInMemory 1000000 \
         -LOD 0.4 \
         -known $RESOURCES/Homo_sapiens_assembly19.known_indels_20120518.vcf
```

## Recalibrate base quality

Sequencers output an estimated base quality score for each base in the output. These scores are heavily used by variant calling algorithms. Unfortunately, they are subject to systematic errors. 

The GATK `BaseRecalibrator` tool builds a Machine Learning model of the sequencing error using the input data and a set of known variants 
(`dbsnp135.vcf` and `known_indels_20120518.vcf` in our case), then outputs a report with the following tables:

* The quantized qualities table
* The recalibration table by read group
* The recalibration table by quality score
* The recalibration table for all the optional covariates

### Compute recalibration table

First, we will compute the recalibration table:

```
java -Xmx4G -jar $RESOURCES/tools/GenomeAnalysisTK.jar -T BaseRecalibrator \
         -R $RESOURCES/hg19/Homo_sapiens_assembly19.fasta \
         -I $OUT/aligned.gatk_indel_realigned.bam \
         -L $RESOURCES/variantplex_03012017_targets.bed  \
         --num_cpu_threads_per_data_thread 1 \
         -knownSites $RESOURCES/Homo_sapiens_assembly19.dbsnp135.vcf \
         -knownSites $RESOURCES/Homo_sapiens_assembly19.known_indels_20120518.vcf \
         -o $OUT/base_recalibration.table
```

The recalibration table is human-readable. Try it in the browser.

### Apply recalibration to BAM
 
Now that we have the table, we will use it to adjust base qualities in our BAM:

```
java -jar $RESOURCES/tools/GenomeAnalysisTK.jar -T PrintReads \
         -I $OUT/aligned.gatk_indel_realigned.bam \
         -R $RESOURCES/hg19/Homo_sapiens_assembly19.fasta \
         -BQSR $OUT/base_recalibration.table \
         -o $OUT/aligned.gatk_base_recalibrated.bam
```

### Rename BAM for next step and index

Now let's rename and index the recalibrated BAM for further analysis. `samtools index` will create a `bai` file, which will enable very fast lookup of reads 
by their coordinate -- essential for variant calling, coverage calculation and visualization in IGV.

```
cp $OUT/aligned.gatk_base_recalibrated.bam $OUT/aligned.pre_dup.bam
samtools index $OUT/aligned.pre_dup.bam
```

## Mark duplicates

Next, we use Picard to mark duplicate reads, that is, reads the are considered to be PCR duplicates.  Reads that have the same start and end alignment positions are considered to be PCR duplicates.  Note that this command doesn't actually remove any duplicates.

```
java -Xmx4G -jar $RESOURCES/tools/picard.jar MarkDuplicates \
         INPUT=$OUT/aligned.pre_dup.bam \
         OUTPUT=$OUT/aligned.final.bam \
         REMOVE_DUPLICATES=false \
         METRICS_FILE=$OUT/duplicate_metrics.txt \
         CREATE_INDEX=true
```

You can inspect duplication metrics by viewing `duplicate_metrics.txt` in the web browser.

## Compute coverage for BED targets

Our assay covers targets from the BED file `variantplex_03012017_coverage.bed`. For QC purposes, we want to make sure we have sufficient per-base read depth for those targets.

We use bedtools to compute the average base coverage for each of the targets in the BED file. The `sed` command normalizes line endings and makes the output easier to read but isn't strictly necessary.

```
bedtools coverage -mean -a $RESOURCES/variantplex_03012017_coverage.bed -b $OUT/aligned.final.bam | sed 's/\r//g' > $OUT/coverage.txt
```

## Call variants

Now that the BED file has been fully preprocessed, we are ready to call variants with lofreq:

```
lofreq call \
           --ref $RESOURCES/hg19/Homo_sapiens_assembly19.fasta \
           --bed  $RESOURCES/variantplex_03012017_targets.bed \
           --call-indels \
           --use-orphan \
           $OUT/aligned.final.bam > $OUT/variants.vcf
```

The output is a VCF file `$OUT/variants.vcf`. You can view it with `less` (command below). Note the header/INFO lines, followed by a table of variants.

```
less $OUT/variants.vcf
```


## Annotate variants with VEP

A standard vcf file is structured and tab delimited.  However, it is not very easy for human consumption/reading.  In addition, a raw vcf file lacks additional context and annotation required for human interpretation (involved gene, base, residue, consequence, function, etc.).  We use Variant Effect Predictor from Ensembl to annotate the variants prior to review.  [http://grch37.ensembl.org/Homo_sapiens/Tools/VEP](http://grch37.ensembl.org/Homo_sapiens/Tools/VEP)
```
perl $RESOURCES/tools/ensembl-vep/vep -i $OUT/variants.vcf \
         --assembly GRCh37 \
         --vcf \
         -o $OUT/variants.annotated.vcf \
         --everything \
         --force_overwrite \
         --hgvs \
         --fasta $RESOURCES/Homo_sapiens.GRCh37.75.dna.primary_assembly.fa \
         --dir $RESOURCES/.vep \
         --no_stats \
         --force_overwrite \
         --offline \
         --cache \
         --pick_order canonical,appris,tsl,biotype,ccds,rank,length \
         --pick
```

## Convert VEP-annotated VCF to Excel
```
python3 /data/resources/tools/vep_to_excel.py \
            $OUT/variants.annotated.vcf \
            $OUT/aligned.final.bam \
            $OUT/variants.annotated.xlsx
```


## Copy key files into a final directory
```
mkdir -p $OUT/final
cp -rv $OUT/qc $OUT/final
cp -rv $OUT/duplicate_metrics.txt $OUT/final
cp -rv $OUT/coverage.txt $OUT/final
cp -rv $OUT/aligned.final.bam $OUT/final
cp -rv $OUT/aligned.final.bai $OUT/final
cp -rv $OUT/variants.annotated.vcf $OUT/final
cp -rv $OUT/variants.annotated.xlsx $OUT/final
```

## Visualize results
Navigate to your group directory in a Web browser: [http://hms-teaching.cidilab.org/groupX/](http://hms-teaching.cidilab.org/)

Locate the Excel file under `final` to view the variants. If you have IGV open, use the "IGV" column to view pileups for each variant.  To create links to IGV in Excel, (1) create a new column1, (2) paste in a hyperlink formula startng with cell A2 [=hyperlink(B2,"igv")], (3) fill the formula down in column1 to the last variant.  Clicking on the "igv" links per variant should activate IGV to load the portion of the bam file from your group web directory.

You will need IGV locally installed/running on your laptop/computer.  Installation instructions may be found here [https://software.broadinstitute.org/software/igv/download](https://software.broadinstitute.org/software/igv/download).
